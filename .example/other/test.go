package main

import (
	"fmt"
	"github.com/gogf/gf/text/gstr"
)

func main() {
	fmt.Println(gstr.CamelCase("/auth/routerName.html"))
}
